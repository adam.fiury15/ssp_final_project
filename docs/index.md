# SSP final project

## Build

```
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Run

```
python ./main
```

[Pylint](https://adam.fiury15.gitlab.io/ssp_final_project/pylint.html)
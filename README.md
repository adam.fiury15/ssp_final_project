[![pipeline status](https://gitlab.com/adam.fiury15/ssp_final_project/badges/main/pipeline.svg)](https://gitlab.com/adam.fiury15/ssp_final_project/-/commits/main)

# SSP final project

Created by Karolina Mikulova and Adam Fiury.

The project was developed to test working with the OpenCV library.

## How to use

```bash
apt-get update -qy
apt-get install -y libgl1-mesa-glx
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## How to run

```bash
python ./main
```